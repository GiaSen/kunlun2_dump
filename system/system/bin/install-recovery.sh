#!/system/bin/sh
if ! applypatch --check EMMC:/dev/block/bootdevice/by-name/recovery:67108864:de5d7fbbbc0182f725b13932a6241e0ab455e965; then
  applypatch  \
          --patch /system/recovery-from-boot.p \
          --source EMMC:/dev/block/bootdevice/by-name/boot:67108864:29a9ca5c01862a5d47172599ade24f973e16e6ae \
          --target EMMC:/dev/block/bootdevice/by-name/recovery:67108864:de5d7fbbbc0182f725b13932a6241e0ab455e965 && \
      log -t recovery "Installing new recovery image: succeeded" || \
      log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi
